package CardGame;

public class Player {
    String name;
    Card[] hand;

    Player(String name) {
        this.name = name;
        this.hand = new Card[0];
    }

    void addCardToHand(Card card) {
        hand = addCardToHand(hand, card);
    }

    Card[] getHand() {
        return hand;
    }

    String getName() {
        return name;
    }

    private Card[] addCardToHand(Card[] hand, Card card) {
        Card[] newHand = new Card[hand.length + 1];

        for (int i = 0; i < hand.length; i++) {
            newHand[i] = hand[i];
        }

        newHand[newHand.length - 1] = card;

        return newHand;
    }
}
