package CardGame;

import java.util.Scanner;

public class Game {

    public static String getHandString(Card[] hand) {
        String result = "";
    
        for (int i = 0; i < hand.length; i++) {
            result += hand[i].toString();
        }
    
        return result;
    }

    public static int getHandValue(Card[] hand) {
        int sum = 0;
        boolean hasAce = false;

        for (Card card : hand) {
            sum += card.getValue();
            if (card.getValue() == 11) {
                hasAce = true;
            }
        }

        while (sum > 21 && hasAce) {
            sum -= 10;
            hasAce = false;
        }

        return sum;
    }

    public void playPlayerTurn(Scanner input, Player player, Deck deck, Player dealer) {
    int playerSum = getHandValue(player.getHand());

    while (true) {
        System.out.println(player.getName() + "'s hand: \n" + getHandString(player.getHand()));
        System.out.println(player.getName() + "'s sum: " + playerSum + "\n\n");
        System.out.println("Dealer's up card: \n" + dealer.getHand()[0]);

        System.out.print(player.getName() + ", do you want to hit or stand? ");
        String choice = input.nextLine();

        if (choice.equalsIgnoreCase("hit")) {
            player.addCardToHand(deck.drawCard());
            Card newCard = player.getHand()[player.getHand().length - 1];
            playerSum += newCard.getValue();

            System.out.println(player.getName() + " received: \n" + newCard);
            System.out.println(player.getName() + "'s new sum: " + playerSum);

            if (playerSum > 21) {
                System.out.println(player.getName() + " busts!");
                break;
            }
        } else if (choice.equalsIgnoreCase("stand")) {
            break;
        } else {
            System.out.println("Invalid choice. Please try again.");
        }
    }

    // Display the final hand after the player's turn is complete
    System.out.println(player.getName() + "'s final hand: \n" + getHandString(player.getHand()));
}


    public void playDealerTurn(Player dealer, Deck deck) {
        int dealerSum = getHandValue(dealer.getHand());

        while (dealerSum < 17) {
            dealer.addCardToHand(deck.drawCard());
            dealerSum += dealer.getHand()[dealer.getHand().length - 1].getValue();
        }

        System.out.println("Dealer's hand: \n" + getHandString(dealer.getHand()));
        System.out.println("Dealer's sum: " + dealerSum);
    }

    public void determineWinner(Player player1, Player player2, Player dealer) {
        int dealerSum = getHandValue(dealer.getHand());
        int player1Sum = getHandValue(player1.getHand());
        int player2Sum = getHandValue(player2.getHand());

        System.out.println("Player 1's sum: " + player1Sum);
        System.out.println("Player 2's sum: " + player2Sum);


        if (player1Sum > 21) {
            if (player2Sum > dealerSum && player2Sum <= 21) {
                System.out.println(player1.getName() + " busts! Player 2 wins!");
            } else if(player2Sum > dealerSum && player2Sum>21) {
                System.out.println(player1.getName() + "and"+ player2.getName() + " busts! Dealer wins!");
            }
            else if(player2Sum<dealerSum && dealerSum<=21){
                 System.out.println(player1.getName() + " busts! Dealer wins!");
            }
            else if(player2Sum<dealerSum && dealerSum>21){
                 System.out.println(player1.getName() + " and dealer busts! Player 2 wins!");
            }
            else{
                System.out.println(player1.getName() + " busts! Its a tie between Player 2 and Dealer!");
            }
        } else if (player2Sum > 21) {
            if (player1Sum > dealerSum && player1Sum <= 21) {
                System.out.println(player2.getName() + " busts! Player 1 wins!");
            } else if(player1Sum > dealerSum && player1Sum>21) {
                System.out.println(player2.getName() + "and"+ player1.getName() + " busts! Dealer wins!");
            }
            else if(player1Sum<dealerSum && dealerSum<=21){
                 System.out.println(player2.getName() + " busts! Dealer wins!");
            }
            else if(player1Sum<dealerSum && dealerSum>21){
                 System.out.println(player2.getName() + " and dealer busts! Player 1 wins!");
            }
            else{
                System.out.println(player2.getName() + " busts! Its a tie between Player 1 and Dealer!");
            }
        } else if (dealerSum > 21) {
            System.out.println("Dealer busts! Both players win!");
        }
         else if (player1Sum > dealerSum && player2Sum > dealerSum) {
            System.out.println("Both players win!");
        } else if (player1Sum > dealerSum) {
            System.out.println(player1.getName() + " wins!");
        } else if (player2Sum > dealerSum) {
            System.out.println(player2.getName() + " wins!");
        } else if(dealerSum>player1Sum && dealerSum>player2Sum){
            System.out.println("Dealer wins!");
        }else {
            System.out.println("It's a tie!");
        }
    }
}
