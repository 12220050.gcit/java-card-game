package CardGame;
import java.util.Scanner;
// import CardGame.Card;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Collecting player's name
        System.out.print("Enter your name: ");
        String playerName = scanner.nextLine();
        Player player = new Player(playerName);

        // Choosing opponent: friend or computer
        System.out.println("Choose your opponent:");
        System.out.println("1. Play with a friend");
        System.out.println("2. Play with the computer");
        int opponentChoice = scanner.nextInt();
        scanner.nextLine(); // Consume newline character

        Player opponent;
        if (opponentChoice == 1) {
            System.out.print("Enter your friend's name: ");
            String opponentName = scanner.nextLine();
            opponent = new Player(opponentName);
        } else {
            // Create logic to play against the computer
            // For simplicity, you can create an instance of a computer player
            opponent = new Player("Computer");
        }

        // Game options
        System.out.println("Choose game mode:");
        System.out.println("1. Play all out");
        System.out.println("2. Flux");
        int gameMode = scanner.nextInt();

        // Logic for different game modes (playAllOut, flux)
        if (gameMode == 1) {
            playAllOut(player.getName(), opponent.getName());
        } else if (gameMode == 2) {
            flux(player.getName(), opponent.getName());
        } else {
            System.out.println("Invalid choice. Exiting game.");
        }

        // Close scanner at the end
        scanner.close();
    }
    
    private static void playAllOut(String playerName, String opponentName) {
        Deck deck = new Deck();
        deck.initializeDeck();
        deck.shuffle();
    
        Player player = new Player(playerName);
        Player opponent = new Player(opponentName);
    
        int playerWins = 0;
        int opponentWins = 0;
        int rounds = 0;
    
        while (player.getHand().length < 52 / 2 || opponent.getHand().length < 52 / 2) {
            if (player.getHand().length < 52 / 2) {
                player.addCardToHand(deck.drawCard());
            }
            if (opponent.getHand().length < 52 / 2) {
                opponent.addCardToHand(deck.drawCard());
            }
        }
    
        for (int i = 0; i < player.getHand().length; i++) {
            Card playerCard = player.getHand()[i];
            Card opponentCard = opponent.getHand()[i];

            
    
            System.out.println("Round " + (i + 1));
            System.out.println(playerName + " drew: ");
            System.out.println(playerCard);
            System.out.println(opponentName + " drew: ");
            System.out.println(opponentCard);
    
            if (playerCard.rank > opponentCard.rank) {
                System.out.println(playerName + " wins this round!");
                playerWins++;
            } else if (playerCard.rank < opponentCard.rank) {
                System.out.println(opponentName + " wins this round!");
                opponentWins++;
            } else {
                System.out.println("It's a tie for this round!");
            }
            
            // System.out.println();
            System.out.println(playerName + " has won : " + playerWins + " rounds. " + " | " + opponentName + " has won: " + opponentWins + " rounds. ");
            System.out.println();
    
        }
    
        if (playerWins > opponentWins) {
            System.out.println(playerName + " wins the game with " + playerWins + " wins!");
        } else if (playerWins < opponentWins) {
            System.out.println(opponentName + " wins the game with " + opponentWins + " wins!");
        } else {
            System.out.println("It's a tie! Both players have the same number of wins.");
        }
    }
    
    
    
    private static void flux(String playerName, String opponentName) {
        Player player = new Player(playerName);
        Player opponent = new Player(opponentName);
    
        int playerWins = 0;
        int opponentWins = 0;
        int rounds = 0;
    
        while (rounds < 1) { // Set the game round to three
            Deck deck = new Deck();
            deck.initializeDeck();
            deck.shuffle(); // Shuffle the deck before each round

            for (int i = 0; i < 3; i++) { // Deal 3 cards to each player
                player.addCardToHand(deck.drawCard());
                opponent.addCardToHand(deck.drawCard());
            }
            // ========================================
            // System.out.println("Press Enter to start the game...");
            // Scanner scanner = new Scanner(System.in);
            // ==========================================
            System.out.println("Round " + (rounds + 1));
            System.out.println(playerName + "'s cards:");
            displayCards(player.getHand());
            System.out.println();
    
            System.out.println(opponentName + "'s cards:");
            displayCards(opponent.getHand());
            System.out.println();


            // =====================================================
            // Check conditions - set of three, run, same suit, pair
            boolean playerSetOfThree = checkSetOfThree(player.getHand());
            boolean opponentSetOfThree = checkSetOfThree(opponent.getHand());
    
            boolean playerRun = checkRun(player.getHand());
            boolean opponentRun = checkRun(opponent.getHand());
    
            boolean playerSameSuit = checkSameSuit(player.getHand());
            boolean opponentSameSuit = checkSameSuit(opponent.getHand());
    
            boolean playerPair = checkPair(player.getHand());
            boolean opponentPair = checkPair(opponent.getHand());
    
            // Compare conditions
            if (playerSetOfThree && !opponentSetOfThree) {
                System.out.println(playerName + " wins with a set of three!");
                playerWins++;
            } else if (opponentSetOfThree && !playerSetOfThree) {
                System.out.println(opponentName + " wins with a set of three!");
                opponentWins++;
            } else if (playerRun && !opponentRun) {
                System.out.println(playerName + " wins with a run!");
                playerWins++;
            } else if (opponentRun && !playerRun) {
                System.out.println(opponentName + " wins with a run!");
                opponentWins++;
            } else if (playerSameSuit && !opponentSameSuit) {
                System.out.println(playerName + " wins with same suit!");
                playerWins++;
            } else if (opponentSameSuit && !playerSameSuit) {
                System.out.println(opponentName + " wins with same suit!");
                opponentWins++;
            } else if (playerPair && !opponentPair) {
                System.out.println(playerName + " wins with a pair!");
                playerWins++;
            } else if (opponentPair && !playerPair) {
                System.out.println(opponentName + " wins with a pair!");
                opponentWins++;
            } else {
                // Compare top card if no condition matches
                int playerTopCard = getTopCard(player.getHand());
                int opponentTopCard = getTopCard(opponent.getHand());
    
                if (playerTopCard > opponentTopCard) {
                    System.out.println(playerName + " wins with the highest card!");
                    playerWins++;
                } else if (playerTopCard < opponentTopCard) {
                    System.out.println(opponentName + " wins with the highest card!");
                    opponentWins++;
                } else {
                    System.out.println("It's a tie for this round!");
                }
            }
    
            rounds++;
        }
    
        // Determine the winner based on round wins
        // if (playerWins > opponentWins) {
        //     System.out.println(playerName + " wins the game with " + playerWins + " round wins!");
        // } else if (playerWins < opponentWins) {
        //     System.out.println(opponentName + " wins the game with " + opponentWins + " round wins!");
        // } else {
        //     System.out.println("It's a tie! Both players have the same number of round wins.");
        // }
    }
    
    private static boolean checkSetOfThree(Card[] hand) {
        int[] rankFrequency = new int[14]; // Array to count the frequency of each rank (from 1 to 13)
    
        for (Card card : hand) {
            rankFrequency[card.rank]++;
        }
    
        for (int i = 1; i <= 13; i++) {
            if (rankFrequency[i] >= 3) {
                return true; // Found a set of three
            }
        }
        return false; // No set of three found
    }
    
    private static void displayCards(Card[] hand) {
        for (Card card : hand) {
            System.out.print(card + "");
        }
    }
    
    private static boolean checkRun(Card[] hand) {
        int[] rankFrequency = new int[14]; // Array to count the frequency of each rank (from 1 to 13)
    
        for (Card card : hand) {
            rankFrequency[card.rank]++;
        }
    
        for (int i = 1; i <= 11; i++) {
            if (rankFrequency[i] > 0 && rankFrequency[i + 1] > 0 && rankFrequency[i + 2] > 0) {
                return true; // Found a run of three consecutive ranks
            }
        }
        return false; // No run found
    }
    
    
    private static boolean checkSameSuit(Card[] hand) {
        int[] suitFrequency = new int[4]; // Array to count the frequency of each suit (0 to 3)
    
        for (Card card : hand) {
            suitFrequency[card.suit]++;
        }
    
        for (int frequency : suitFrequency) {
            if (frequency >= 3) {
                return true; // Found three cards of the same suit
            }
        }
        return false; // No same suit found
    }
    
    
    private static boolean checkPair(Card[] hand) {
        int[] rankFrequency = new int[14]; // Array to count the frequency of each rank (from 1 to 13)
    
        for (Card card : hand) {
            rankFrequency[card.rank]++;
        }
    
        for (int i = 1; i <= 13; i++) {
            if (rankFrequency[i] >= 2) {
                return true; // Found a pair
            }
        }
        return false; // No pair found
    }
    
    private static int getTopCard(Card[] hand) {
        int maxRank = 0;
    
        for (Card card : hand) {
            if (card.rank > maxRank) {
                maxRank = card.rank;
            }
        }
        return maxRank;
    }
}
