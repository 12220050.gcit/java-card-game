package CardGame;

public class Deck {

     Card[] cards;
    private int stackSize;
    private int deckIndex;

    Deck() {
        this.cards = new Card[52];
        this.stackSize = 0;
        this.deckIndex = 0;
    }

    void initializeDeck() {
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                cards[stackSize++] = new Card(i, j);
            }
        }

        deckIndex = 0;
    }

    void shuffle() {
        Card[] shuffledDeck = new Card[stackSize];

        for (int i = 0; i < stackSize; i++) {
            int randIndex = (int) (Math.random() * (i + 1));
            shuffledDeck[i] = shuffledDeck[randIndex];
            shuffledDeck[randIndex] = cards[i];
        }

        stackSize = shuffledDeck.length;
        cards = shuffledDeck;
    }

    Card drawCard() {
        if (deckIndex < stackSize) {
            return cards[deckIndex++];
        } else {
            System.out.println("The deck is empty.");
            return null;
        }
    }

    public int getStackSize() {
        return stackSize;
    }
    
}
