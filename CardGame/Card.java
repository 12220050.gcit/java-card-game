package CardGame;


public class Card {
    int suit;
    int rank;
    
    private static Card[] cardHash = new Card[52];

    Card(int suit, int rank) {
        this.suit = suit;
        this.rank = rank;
        
        int hashIndex = calculateHashIndex(suit, rank);
        if (cardHash[hashIndex] == null) {
            cardHash[hashIndex] = this;
        }
    }

    private int calculateHashIndex(int suit, int rank) {
        return suit * 13 + rank - 1;
    }

    int getValue() {
        if (rank == 1) {
            return 11;
        } else if (rank >= 10) {
            return 10;
        } else {
            return rank;
        }
    }

    public String toString() {
        String[] suitSymbols = {"HEART", "SPADE", "CLUB", "DIAMOND"};
        String[] rankStrings = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
    
        String suitSymbol = (suit >= 0 && suit < suitSymbols.length) ? suitSymbols[suit] : " "; // Changed ' ' to " "
        String rankString = (rank >= 1 && rank < rankStrings.length) ? rankStrings[rank] : Integer.toString(rank);
    
        if (rank == 10) {
            return "┌───────────────┐\n" +
                    "  " + rankString + "             \n" +
                    "  " + "                \n" +
                    "       " + suitSymbol + "            \n" +
                    "  " + "                \n" +
                    "             " + rankString +     "   \n" +
                    "└──────────────┘ \n \n";
        }
        return "┌───────────────┐\n" +
                " " + rankString + "            \n" +
                " " + "               \n" +
                "      " + suitSymbol + "           \n" +
                " " + "              \n" +
                "             " + rankString + "      \n" +
                "└──────────────┘ \n \n";
    }
    

    public static Card getCard(int suit, int rank) {
        int hashIndex = suit * 13 + rank - 1;
        return cardHash[hashIndex];
    }
}




