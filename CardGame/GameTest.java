package CardGame;

public class GameTest {
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.initializeDeck();
        for (int i =0 ; i < deck.getStackSize();i++){
            System.out.print(deck.cards[i]);
        }
        System.out.println("Shuffled cards:");
        deck.shuffle();
         for (int i =0 ; i < deck.getStackSize();i++){
            System.out.print(deck.cards[i] + " ");
        }

        System.out.println(deck.getStackSize());


        System.out.println(deck.drawCard());
        System.out.println(deck.drawCard().rank);
        System.out.println(deck.drawCard().suit);


    }
}
